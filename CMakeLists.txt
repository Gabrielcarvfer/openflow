cmake_minimum_required(VERSION 3.13..3.13)

project(openflow)

set(CMAKE_POSITION_INDEPENDENT_CODE ON) # fpic + fpie

find_package(LibXml2 QUIET)
if(NOT ${LIBXML2_FOUND})
    message(FATAL_ERROR "LibXML2 is required by Openflow")
endif()

set(source_files
        lib/command-line.c
        lib/csum.c
        lib/daemon.c
        lib/dhcp-client.c
        lib/dhcp.c
        lib/dirs.c
        lib/dpif.c
        lib/dynamic-string.c
        lib/fatal-signal.c
        lib/fault.c
        lib/flow.c
        lib/freelist.c
        lib/hash.c
        lib/hmap.c
        lib/learning-switch.c
        lib/list.c
        lib/mac-learning.c
        lib/misc.c
        lib/mpls-fib.c
        lib/mpls-switch.c
        lib/netdev.c
        lib/netlink.c
        lib/ofp-print.c
        lib/ofpbuf.c
        lib/poll-loop.c
        lib/port-array.c
        lib/queue.c
        lib/random.c
        lib/rconn.c
        lib/read-mpls-fib.c
        lib/red-black-tree.c
        lib/signals.c
        lib/socket-util.c
        lib/stack.c
        lib/stp.c
        lib/timeval.c
        lib/util.c
        lib/vconn-mpls.c
        lib/vconn-netlink.c
        lib/vconn-stream.c
        lib/vconn-tcp.c
        lib/vconn-unix.c
        lib/vconn.c
        lib/vlog-socket.c
        lib/vlog.c
        switch/chain.c
        switch/crc32.c
        switch/datapath.c
        switch/dp_act.c
        switch/er_act.c
        switch/nx_act.c
        switch/pt_act.c
        switch/switch-flow.c
        switch/switch-port.c
        switch/switch.c
        switch/table-hash.c
        switch/table-linear.c
)

set(private_header_files
        lib/compiler.h
        lib/csum.h
        lib/flow.h
        lib/list.h
        lib/misc.h
        lib/ofpbuf.h
        lib/packets.h
        lib/poll-loop.h
        lib/queue.h
        lib/random.h
        lib/rconn.h
        lib/red-black-tree.h
        lib/stack.h
        lib/stp.h
        lib/timeval.h
        lib/type-props.h
        lib/util.h
        lib/vconn.h
        lib/xtoxll.h
        switch/chain.h
        switch/datapath.h
        switch/dp_act.h
        switch/pt_act.h
        switch/switch-flow.h
        switch/switch-port.h
        switch/table.h
)

add_library(openflow STATIC ${source_files})
add_library(openflow::openflow ALIAS openflow)

target_include_directories(openflow PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR}
        ${CMAKE_CURRENT_SOURCE_DIR}/include
        ${CMAKE_CURRENT_SOURCE_DIR}/lib
)
target_compile_definitions(openflow PUBLIC -DNS3)
target_link_libraries(openflow PUBLIC LibXml2::LibXml2)

include(GNUInstallDirs)

install(
        TARGETS openflow
        EXPORT openflowExportTargets
        ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}/
        LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}/
        RUNTIME DESTINATION ${CMAKE_INSTALL_LIBDIR}/
)

install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/include/openflow
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/
        FILES_MATCHING
        PATTERN "*.h")

install(FILES ${private_header_files}
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/openflow/private)